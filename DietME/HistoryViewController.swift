//
//  HistoryViewController.swift
//  DietME
//
//  Created by Fallah on 4/2/17.
//  Copyright © 2017 Wentworth. All rights reserved.
//

import UIKit
import HealthKit
import SwiftSpinner

class HistoryViewController: UITableViewController,HistoryHeaderCellProtocol {
    
    let historyReuseID = "HistoryCell"
    let nutritionReuseID = "NutritionCell"
    var apiData: DietAPI?
    
    var date = Date()
    
    var currentCount = 0
    
    var foodsToDisplay = [Food]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "HistoryHeaderCell", bundle: nil), forCellReuseIdentifier: historyReuseID)
        tableView.register(UINib(nibName: "NutritionCell", bundle: nil), forCellReuseIdentifier: nutritionReuseID)
        
        
        fetchFoodFromThatDay(date: Date()) { (food: [Food]?) in
            guard let foo = food else {
                self.foodsToDisplay = []
                return
            }
            
            self.foodsToDisplay = foo
            self.tableView.reloadData()
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {

    }
    
    func fetchFoodFromThatDay(date: Date, completion: @escaping (_ foods: [Food]? ) -> Void)  {
        HealthManager.getFoodNamesFromDay(day: date)
        { (foodNames: [String]) in
            guard !foodNames.isEmpty else {
                return
            }
                SwiftSpinner.show("Fetching")
            HealthLoggerUTIL.searchForFood(foodFromSpeech: foodNames.joined(separator: ",")) {
                (apiResult: DietAPI?) in
                
                defer {
                    SwiftSpinner.hide()
                    
                }
                
                guard let result = apiResult else {
                    //SHOW AN ERROR
                    return
                }
                
                completion(result.foods)
                
            }

        }
        
        
    }
    
    func didTapBack() {
        
        currentCount -= 1
        let calendar = NSCalendar.current
        
        date = calendar.date(byAdding: .day, value: -1, to: date)!
        
        print(date.toString(style: .month))
        
        fetchFoodFromThatDay(date: date) { (foods: [Food]?) in
            guard let food = foods else {
                return
            }
            
            self.foodsToDisplay = food
            self.tableView.reloadData()
        }
        
    }
    
    func didTapForward() {
        
        currentCount += 1
        let calendar = NSCalendar.current
        
        date = calendar.date(byAdding: .day, value: 1, to: date)!
        fetchFoodFromThatDay(date: date) { (foods: [Food]?) in
            guard let food = foods else {
                return
            }
            
            self.foodsToDisplay = food
            self.tableView.reloadData()
        }
         
        
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 + self.foodsToDisplay.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: historyReuseID, for: indexPath) as! HistoryHeaderCell
            cell.delegate = self
            
            cell.initCell(date: date)
            
            if currentCount == 0 {
                cell.nextDayButton.isEnabled = false
            } else {
                cell.nextDayButton.isEnabled = true
            }
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: nutritionReuseID, for: indexPath) as! NutritionCell
            cell.initCell(food: foodsToDisplay[indexPath.row+1])
            return cell
        }
     
    }
 
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
