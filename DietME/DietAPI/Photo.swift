//
//	Photo.swift
//
//	Create by Alex Fallah on 1/4/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Photo : NSObject, NSCoding{

	var highres : String!
	var thumb : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		highres = dictionary["highres"] as? String
		thumb = dictionary["thumb"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if highres != nil{
			dictionary["highres"] = highres
		}
		if thumb != nil{
			dictionary["thumb"] = thumb
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         highres = aDecoder.decodeObject(forKey: "highres") as? String
         thumb = aDecoder.decodeObject(forKey: "thumb") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if highres != nil{
			aCoder.encode(highres, forKey: "highres")
		}
		if thumb != nil{
			aCoder.encode(thumb, forKey: "thumb")
		}

	}

}