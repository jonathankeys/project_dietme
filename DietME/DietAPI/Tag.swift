//
//	Tag.swift
//
//	Create by Alex Fallah on 1/4/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Tag : NSObject, NSCoding{

	var item : String!
	var measure : AnyObject!
	var quantity : String!
	var tagId : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		item = dictionary["item"] as? String
		measure = dictionary["measure"] as? AnyObject
		quantity = dictionary["quantity"] as? String
		tagId = dictionary["tag_id"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if item != nil{
			dictionary["item"] = item
		}
		if measure != nil{
			dictionary["measure"] = measure
		}
		if quantity != nil{
			dictionary["quantity"] = quantity
		}
		if tagId != nil{
			dictionary["tag_id"] = tagId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         item = aDecoder.decodeObject(forKey: "item") as? String
         measure = aDecoder.decodeObject(forKey: "measure") as? AnyObject
         quantity = aDecoder.decodeObject(forKey: "quantity") as? String
         tagId = aDecoder.decodeObject(forKey: "tag_id") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if item != nil{
			aCoder.encode(item, forKey: "item")
		}
		if measure != nil{
			aCoder.encode(measure, forKey: "measure")
		}
		if quantity != nil{
			aCoder.encode(quantity, forKey: "quantity")
		}
		if tagId != nil{
			aCoder.encode(tagId, forKey: "tag_id")
		}

	}

}