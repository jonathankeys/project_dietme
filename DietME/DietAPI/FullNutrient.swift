//
//	FullNutrient.swift
//
//	Create by Alex Fallah on 1/4/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FullNutrient : NSObject, NSCoding{

	var attrId : Int!
	var value : Float!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		attrId = dictionary["attr_id"] as? Int
		value = dictionary["value"] as? Float
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if attrId != nil{
			dictionary["attr_id"] = attrId
		}
		if value != nil{
			dictionary["value"] = value
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         attrId = aDecoder.decodeObject(forKey: "attr_id") as? Int
         value = aDecoder.decodeObject(forKey: "value") as? Float

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if attrId != nil{
			aCoder.encode(attrId, forKey: "attr_id")
		}
		if value != nil{
			aCoder.encode(value, forKey: "value")
		}

	}

}