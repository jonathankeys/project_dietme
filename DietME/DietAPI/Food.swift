//
//	Food.swift
//
//	Create by Alex Fallah on 1/4/2017
//	Copyright © 2017. All rights reserved.


import Foundation


class Food : NSObject, NSCoding{
    
    var altMeasures : [AltMeasure]!
    var brandName : AnyObject!
    var consumedAt : String!
    var foodName : String!
    var fullNutrients : [FullNutrient]!
    var lat : AnyObject!
    var lng : AnyObject!
    var mealType : Int!
    var metadata : Metadata!
    var ndbNo : Int!
    
    var nfCalories : Float!
    var nfCholesterol : Float!
    var nfDietaryFiber : Float!
    var nfP : Float!
    var nfPotassium : Float!
    var nfProtein : Float!
    var nfSaturatedFat : Float!
    var nfSodium : Float!
    var nfSugars : Float!
    var nfTotalCarbohydrate : Float!
    var nfTotalFat : Float!
    var servingQty : Int!
    var servingUnit : String!
    var servingWeightGrams : Float!
    
    var nixBrandId : AnyObject!
    var nixBrandName : AnyObject!
    var nixItemId : AnyObject!
    var nixItemName : AnyObject!
    var photo : Photo!
    var source : Int!
    var tags : Tag!
    var upc : AnyObject!
    
    func adjustServingSize(newServingSize: Int) {

        let originalServing = servingQty //5 servings of wings
        let adjustable = newServingSize - servingQty // Did we increment or decrement? adjust = + or - 1
        
        if adjustable == 0 {
            return
        }
        
        if nfCalories != nil {
            nfCalories =  nfCalories + ((nfCalories/Float(originalServing!)) * Float(adjustable))
        } else {
            nfCalories = 0
        }
        
        if nfCholesterol != nil {
            nfCholesterol = nfCholesterol + ((nfCholesterol/Float(originalServing!)) * Float(adjustable))
        } else {
            nfCholesterol = 0
        }
        
        if nfDietaryFiber != nil {
            nfDietaryFiber = nfDietaryFiber + ((nfDietaryFiber/Float(originalServing!)) * Float(adjustable))
        } else {
            nfDietaryFiber = 0
        }
        
        if nfP != nil {
            nfP = nfP + ((nfP/Float(originalServing!)) * Float(adjustable))
        } else {
            nfP = 0
        }
        
        if nfPotassium != nil {
            nfPotassium = nfPotassium + ((nfPotassium/Float(originalServing!)) * Float(adjustable))
        } else {
            nfPotassium = 0
        }
        
        if nfProtein != nil {
            nfProtein = nfProtein + ((nfProtein/Float(originalServing!)) * Float(adjustable))
        } else {
            nfProtein = 0
        }
        
        if nfTotalFat != nil {
            nfTotalFat = nfTotalFat + ((nfTotalFat/Float(originalServing!)) * Float(adjustable))
        } else {
            nfTotalFat = 0
        }
        
        if servingWeightGrams != nil {
            servingWeightGrams = servingWeightGrams + ((servingWeightGrams/Float(originalServing!)) * Float(adjustable))
        } else {
            servingWeightGrams = 0
        }
        
        if nfTotalCarbohydrate != nil {
            nfTotalCarbohydrate = nfTotalCarbohydrate + ((nfTotalCarbohydrate/Float(originalServing!)) * Float(adjustable))
        } else {
            nfTotalCarbohydrate = 0
        }
        
        if nfSodium != nil {
            nfSodium = nfSodium + ((nfSodium/Float(originalServing!)) * Float(adjustable))
        } else {
            nfSodium = 0
        }
        
        if nfSugars != nil {
            nfSugars = nfSugars + ((nfSugars/Float(originalServing!)) * Float(adjustable))
        } else {
            nfSugars = 0
        }
        
        if nfSaturatedFat != nil {
            nfSaturatedFat = nfSaturatedFat + ((nfSaturatedFat/Float(originalServing!)) * Float(adjustable))
        } else {
            nfSaturatedFat = 0
        }
        
        
        servingQty = servingQty + adjustable

    }

    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        altMeasures = [AltMeasure]()
        if let altMeasuresArray = dictionary["alt_measures"] as? [[String:Any]]{
            for dic in altMeasuresArray{
                let value = AltMeasure(fromDictionary: dic)
                altMeasures.append(value)
            }
        }
        brandName = dictionary["brand_name"] as? AnyObject
        consumedAt = dictionary["consumed_at"] as? String
        foodName = dictionary["food_name"] as? String
        fullNutrients = [FullNutrient]()
        if let fullNutrientsArray = dictionary["full_nutrients"] as? [[String:Any]]{
            for dic in fullNutrientsArray{
                let value = FullNutrient(fromDictionary: dic)
                fullNutrients.append(value)
            }
        }
        lat = dictionary["lat"] as? AnyObject
        lng = dictionary["lng"] as? AnyObject
        mealType = dictionary["meal_type"] as? Int
        if let metadataData = dictionary["metadata"] as? [String:Any]{
            metadata = Metadata(fromDictionary: metadataData)
        }
        ndbNo = dictionary["ndb_no"] as? Int
        nfCalories = dictionary["nf_calories"] as? Float
        nfCholesterol = dictionary["nf_cholesterol"] as? Float
        nfDietaryFiber = dictionary["nf_dietary_fiber"] as? Float
        nfP = dictionary["nf_p"] as? Float
        nfPotassium = dictionary["nf_potassium"] as? Float
        nfProtein = dictionary["nf_protein"] as? Float
        nfSaturatedFat = dictionary["nf_saturated_fat"] as? Float
        nfSodium = dictionary["nf_sodium"] as? Float
        nfSugars = dictionary["nf_sugars"] as? Float
        nfTotalCarbohydrate = dictionary["nf_total_carbohydrate"] as? Float
        nfTotalFat = dictionary["nf_total_fat"] as? Float
        nixBrandId = dictionary["nix_brand_id"] as? AnyObject
        nixBrandName = dictionary["nix_brand_name"] as? AnyObject
        nixItemId = dictionary["nix_item_id"] as? AnyObject
        nixItemName = dictionary["nix_item_name"] as? AnyObject
        if let photoData = dictionary["photo"] as? [String:Any]{
            photo = Photo(fromDictionary: photoData)
        }
        servingQty = dictionary["serving_qty"] as? Int
        servingUnit = dictionary["serving_unit"] as? String
        servingWeightGrams = dictionary["serving_weight_grams"] as? Float
        source = dictionary["source"] as? Int
        if let tagsData = dictionary["tags"] as? [String:Any]{
            tags = Tag(fromDictionary: tagsData)
        }
        upc = dictionary["upc"] as? AnyObject
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if altMeasures != nil{
            var dictionaryElements = [[String:Any]]()
            for altMeasuresElement in altMeasures {
                dictionaryElements.append(altMeasuresElement.toDictionary())
            }
            dictionary["alt_measures"] = dictionaryElements
        }
        if brandName != nil{
            dictionary["brand_name"] = brandName
        }
        if consumedAt != nil{
            dictionary["consumed_at"] = consumedAt
        }
        if foodName != nil{
            dictionary["food_name"] = foodName
        }
        if fullNutrients != nil{
            var dictionaryElements = [[String:Any]]()
            for fullNutrientsElement in fullNutrients {
                dictionaryElements.append(fullNutrientsElement.toDictionary())
            }
            dictionary["full_nutrients"] = dictionaryElements
        }
        if lat != nil{
            dictionary["lat"] = lat
        }
        if lng != nil{
            dictionary["lng"] = lng
        }
        if mealType != nil{
            dictionary["meal_type"] = mealType
        }
        if metadata != nil{
            dictionary["metadata"] = metadata.toDictionary()
        }
        if ndbNo != nil{
            dictionary["ndb_no"] = ndbNo
        }
        if nfCalories != nil{
            dictionary["nf_calories"] = nfCalories
        }
        if nfCholesterol != nil{
            dictionary["nf_cholesterol"] = nfCholesterol
        }
        if nfDietaryFiber != nil{
            dictionary["nf_dietary_fiber"] = nfDietaryFiber
        }
        if nfP != nil{
            dictionary["nf_p"] = nfP
        }
        if nfPotassium != nil{
            dictionary["nf_potassium"] = nfPotassium
        }
        if nfProtein != nil{
            dictionary["nf_protein"] = nfProtein
        }
        if nfSaturatedFat != nil{
            dictionary["nf_saturated_fat"] = nfSaturatedFat
        }
        if nfSodium != nil{
            dictionary["nf_sodium"] = nfSodium
        }
        if nfSugars != nil{
            dictionary["nf_sugars"] = nfSugars
        }
        if nfTotalCarbohydrate != nil{
            dictionary["nf_total_carbohydrate"] = nfTotalCarbohydrate
        }
        if nfTotalFat != nil{
            dictionary["nf_total_fat"] = nfTotalFat
        }
        if nixBrandId != nil{
            dictionary["nix_brand_id"] = nixBrandId
        }
        if nixBrandName != nil{
            dictionary["nix_brand_name"] = nixBrandName
        }
        if nixItemId != nil{
            dictionary["nix_item_id"] = nixItemId
        }
        if nixItemName != nil{
            dictionary["nix_item_name"] = nixItemName
        }
        if photo != nil{
            dictionary["photo"] = photo.toDictionary()
        }
        if servingQty != nil{
            dictionary["serving_qty"] = servingQty
        }
        if servingUnit != nil{
            dictionary["serving_unit"] = servingUnit
        }
        if servingWeightGrams != nil{
            dictionary["serving_weight_grams"] = servingWeightGrams
        }
        if source != nil{
            dictionary["source"] = source
        }
        if tags != nil{
            dictionary["tags"] = tags.toDictionary()
        }
        if upc != nil{
            dictionary["upc"] = upc
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        altMeasures = aDecoder.decodeObject(forKey :"alt_measures") as? [AltMeasure]
        brandName = aDecoder.decodeObject(forKey: "brand_name") as? AnyObject
        consumedAt = aDecoder.decodeObject(forKey: "consumed_at") as? String
        foodName = aDecoder.decodeObject(forKey: "food_name") as? String
        fullNutrients = aDecoder.decodeObject(forKey :"full_nutrients") as? [FullNutrient]
        lat = aDecoder.decodeObject(forKey: "lat") as? AnyObject
        lng = aDecoder.decodeObject(forKey: "lng") as? AnyObject
        mealType = aDecoder.decodeObject(forKey: "meal_type") as? Int
        metadata = aDecoder.decodeObject(forKey: "metadata") as? Metadata
        ndbNo = aDecoder.decodeObject(forKey: "ndb_no") as? Int
        nfCalories = aDecoder.decodeObject(forKey: "nf_calories") as? Float
        nfCholesterol = aDecoder.decodeObject(forKey: "nf_cholesterol") as? Float
        nfDietaryFiber = aDecoder.decodeObject(forKey: "nf_dietary_fiber") as? Float
        nfP = aDecoder.decodeObject(forKey: "nf_p") as? Float
        nfPotassium = aDecoder.decodeObject(forKey: "nf_potassium") as? Float
        nfProtein = aDecoder.decodeObject(forKey: "nf_protein") as? Float
        nfSaturatedFat = aDecoder.decodeObject(forKey: "nf_saturated_fat") as? Float
        nfSodium = aDecoder.decodeObject(forKey: "nf_sodium") as? Float
        nfSugars = aDecoder.decodeObject(forKey: "nf_sugars") as? Float
        nfTotalCarbohydrate = aDecoder.decodeObject(forKey: "nf_total_carbohydrate") as? Float
        nfTotalFat = aDecoder.decodeObject(forKey: "nf_total_fat") as? Float
        nixBrandId = aDecoder.decodeObject(forKey: "nix_brand_id") as? AnyObject
        nixBrandName = aDecoder.decodeObject(forKey: "nix_brand_name") as? AnyObject
        nixItemId = aDecoder.decodeObject(forKey: "nix_item_id") as? AnyObject
        nixItemName = aDecoder.decodeObject(forKey: "nix_item_name") as? AnyObject
        photo = aDecoder.decodeObject(forKey: "photo") as? Photo
        servingQty = aDecoder.decodeObject(forKey: "serving_qty") as? Int
        servingUnit = aDecoder.decodeObject(forKey: "serving_unit") as? String
        servingWeightGrams = aDecoder.decodeObject(forKey: "serving_weight_grams") as? Float
        source = aDecoder.decodeObject(forKey: "source") as? Int
        tags = aDecoder.decodeObject(forKey: "tags") as? Tag
        upc = aDecoder.decodeObject(forKey: "upc") as? AnyObject
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if altMeasures != nil{
            aCoder.encode(altMeasures, forKey: "alt_measures")
        }
        if brandName != nil{
            aCoder.encode(brandName, forKey: "brand_name")
        }
        if consumedAt != nil{
            aCoder.encode(consumedAt, forKey: "consumed_at")
        }
        if foodName != nil{
            aCoder.encode(foodName, forKey: "food_name")
        }
        if fullNutrients != nil{
            aCoder.encode(fullNutrients, forKey: "full_nutrients")
        }
        if lat != nil{
            aCoder.encode(lat, forKey: "lat")
        }
        if lng != nil{
            aCoder.encode(lng, forKey: "lng")
        }
        if mealType != nil{
            aCoder.encode(mealType, forKey: "meal_type")
        }
        if metadata != nil{
            aCoder.encode(metadata, forKey: "metadata")
        }
        if ndbNo != nil{
            aCoder.encode(ndbNo, forKey: "ndb_no")
        }
        if nfCalories != nil{
            aCoder.encode(nfCalories, forKey: "nf_calories")
        }
        if nfCholesterol != nil{
            aCoder.encode(nfCholesterol, forKey: "nf_cholesterol")
        }
        if nfDietaryFiber != nil{
            aCoder.encode(nfDietaryFiber, forKey: "nf_dietary_fiber")
        }
        if nfP != nil{
            aCoder.encode(nfP, forKey: "nf_p")
        }
        if nfPotassium != nil{
            aCoder.encode(nfPotassium, forKey: "nf_potassium")
        }
        if nfProtein != nil{
            aCoder.encode(nfProtein, forKey: "nf_protein")
        }
        if nfSaturatedFat != nil{
            aCoder.encode(nfSaturatedFat, forKey: "nf_saturated_fat")
        }
        if nfSodium != nil{
            aCoder.encode(nfSodium, forKey: "nf_sodium")
        }
        if nfSugars != nil{
            aCoder.encode(nfSugars, forKey: "nf_sugars")
        }
        if nfTotalCarbohydrate != nil{
            aCoder.encode(nfTotalCarbohydrate, forKey: "nf_total_carbohydrate")
        }
        if nfTotalFat != nil{
            aCoder.encode(nfTotalFat, forKey: "nf_total_fat")
        }
        if nixBrandId != nil{
            aCoder.encode(nixBrandId, forKey: "nix_brand_id")
        }
        if nixBrandName != nil{
            aCoder.encode(nixBrandName, forKey: "nix_brand_name")
        }
        if nixItemId != nil{
            aCoder.encode(nixItemId, forKey: "nix_item_id")
        }
        if nixItemName != nil{
            aCoder.encode(nixItemName, forKey: "nix_item_name")
        }
        if photo != nil{
            aCoder.encode(photo, forKey: "photo")
        }
        if servingQty != nil{
            aCoder.encode(servingQty, forKey: "serving_qty")
        }
        if servingUnit != nil{
            aCoder.encode(servingUnit, forKey: "serving_unit")
        }
        if servingWeightGrams != nil{
            aCoder.encode(servingWeightGrams, forKey: "serving_weight_grams")
        }
        if source != nil{
            aCoder.encode(source, forKey: "source")
        }
        if tags != nil{
            aCoder.encode(tags, forKey: "tags")
        }
        if upc != nil{
            aCoder.encode(upc, forKey: "upc")
        }
        
    }
    
}
