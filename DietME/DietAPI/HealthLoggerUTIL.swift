//
//  HealthLoggerUTIL.swift
//  DietME
//
//  Created by Fallah on 4/1/17.
//  Copyright © 2017 Wentworth. All rights reserved.
//

import Foundation
import Alamofire
import HealthKit

class HealthLoggerUTIL {
    
    
    static func searchForFood(foodFromSpeech: String?, completion : @escaping (DietAPI?) -> Void)  {
        
        guard let food = foodFromSpeech, foodFromSpeech != "" else {
            //DISPLAY AN ERROR
            
            print("Please enter a food name")
            completion(nil)
            return
        }
        
        let header = [
            
            [
                "x-app-id": "10996f71",
                "x-app-key": "4af6913d43879c112cee0e33b9e68d82"
            ],
            
            [
                "x-app-id": "5b887ee2",
                "x-app-key": "e8ebffb50d523c95f17c0204f2fdb0fe"
            ],
            
            [
                "x-app-id": "16b32b6c",
                "x-app-key": "e56d4b0db5ae1a825655eb27b342d78d"
            ],
            
            [
                "x-app-id": "10996f71",
                "x-app-key": "4af6913d43879c112cee0e33b9e68d82"
            ]
        ]
        
        
        let headers: HTTPHeaders = header[Int(arc4random_uniform(3))]
            
//            [
//            "x-app-id": "10996f71",
//            "x-app-key": "4af6913d43879c112cee0e33b9e68d82"
//        ]
        
        let parameter: Parameters = [
            "query": food
        ]
        
        let search = "https://trackapi.nutritionix.com/v2/natural/nutrients"
        
        Alamofire.request(search, method: .post, parameters: parameter, headers: headers).responseJSON { response in
            
            if response.result.description == "FAILURE" {
                print("error")
                //DISPLAY ERROR
                completion(nil)
            }
            
            if let JSON = response.result.value {
                let field = DietAPI(fromDictionary: JSON as! [String : Any])
                
                completion(field)
            }
            
        }
        
    }

    
}
