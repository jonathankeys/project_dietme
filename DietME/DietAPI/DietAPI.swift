//
//	DietAPI.swift
//
//	Create by Alex Fallah on 1/4/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class DietAPI : NSObject, NSCoding{

	var foods : [Food]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		foods = [Food]()
		if let foodsArray = dictionary["foods"] as? [[String:Any]]{
			for dic in foodsArray{
				let value = Food(fromDictionary: dic)
				foods.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if foods != nil{
			var dictionaryElements = [[String:Any]]()
			for foodsElement in foods {
				dictionaryElements.append(foodsElement.toDictionary())
			}
			dictionary["foods"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         foods = aDecoder.decodeObject(forKey :"foods") as? [Food]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if foods != nil{
			aCoder.encode(foods, forKey: "foods")
		}

	}

}