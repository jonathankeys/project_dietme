///
///	AltMeasure.swift
///
///	Create by Alex Fallah on 1/4/2017
///	Copyright © 2017. All rights reserved.
///

import Foundation


class AltMeasure : NSObject, NSCoding{

	var measure : String!
	var qty : Int!
	var seq : Int!
	var servingWeight : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		measure = dictionary["measure"] as? String
		qty = dictionary["qty"] as? Int
		seq = dictionary["seq"] as? Int
		servingWeight = dictionary["serving_weight"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if measure != nil{
			dictionary["measure"] = measure
		}
		if qty != nil{
			dictionary["qty"] = qty
		}
		if seq != nil{
			dictionary["seq"] = seq
		}
		if servingWeight != nil{
			dictionary["serving_weight"] = servingWeight
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         measure = aDecoder.decodeObject(forKey: "measure") as? String
         qty = aDecoder.decodeObject(forKey: "qty") as? Int
         seq = aDecoder.decodeObject(forKey: "seq") as? Int
         servingWeight = aDecoder.decodeObject(forKey: "serving_weight") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if measure != nil{
			aCoder.encode(measure, forKey: "measure")
		}
		if qty != nil{
			aCoder.encode(qty, forKey: "qty")
		}
		if seq != nil{
			aCoder.encode(seq, forKey: "seq")
		}
		if servingWeight != nil{
			aCoder.encode(servingWeight, forKey: "serving_weight")
		}

	}

}
