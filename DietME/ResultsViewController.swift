//
//  ResultsViewController.swift
//  DietME
//
//  Created by Fallah on 4/1/17.
//  Copyright © 2017 Wentworth. All rights reserved.
//

import UIKit


class ResultsViewController: UITableViewController, ResultsCellProtocol {

    var apiData: DietAPI!
    let resultReuseID = "ResultCell"
    
    @IBOutlet weak var logItemsButton: UIBarButtonItem!
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackground()
        self.navigationController?.toolbar.barTintColor = UIColor(patternImage: #imageLiteral(resourceName: "Mask"))
        tableView.rowHeight = UITableViewAutomaticDimension
        setBottomButtons()
        self.title = "Results"
        tableView.estimatedRowHeight = 154
        tableView.register(UINib(nibName: "ResultsCell", bundle: nil), forCellReuseIdentifier: resultReuseID)
    }

    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setToolbarHidden(false, animated: true)
    }
    
    private func setBottomButtons() {
        let font = UIFont.boldSystemFont(ofSize: 18.0)
        
        let things = [logItemsButton,cancelButton]

        for thing in things {
            
            thing?.setTitleTextAttributes([NSForegroundColorAttributeName : UIColor.white, NSFontAttributeName: font ], for: .normal)
        }
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NutritionSegue" {
            let dvc = segue.destination as! NutritionViewController
            dvc.food = apiData.foods[(tableView.indexPathForSelectedRow?.row)!]
        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "NutritionSegue", sender: self)
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return apiData.foods.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: resultReuseID, for: indexPath) as! ResultsCell

        let food = apiData.foods[indexPath.row]
        
        cell.initCell(food: food, index: indexPath)

        cell.delegate = self
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            apiData.foods.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
        
        if apiData.foods.count == 0 {
            self.navigationController?.dismiss(animated: false, completion: nil)
            return
        }
        
        tableView.reloadData() //for index path to update
    }
    
    // MARK: - ResultCellProtcol
    
    func didTapStepper(step: Double, atIndex: IndexPath ) {
        apiData.foods[atIndex.row].adjustServingSize(newServingSize: Int(step))
        tableView.reloadRows(at: [atIndex], with: .none)
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        self.navigationController?.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func logItemsTapped(_ sender: Any) {
        
        for food in apiData.foods {
            HealthManager.logFoodToHealthKit(food: food)
        }
        
        self.performSegue(withIdentifier: "FinishSegue", sender: self)
        
    }
    
    

}
