//
//  SuccessViewController.swift
//  DietME
//
//  Created by Fallah on 4/6/17.
//  Copyright © 2017 Wentworth. All rights reserved.
//

import UIKit

class SuccessViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setBackground()
    }
    @IBAction func openHealthKit(_ sender: Any) {
        UIApplication.shared.open(URL(string: "x-apple-health://nutrition")!, options: [:],
                                  completionHandler: {
                                    (success) in
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    
                                    let initialViewController = storyboard.instantiateViewController(withIdentifier: "Home")
                                    
                                    self.present(initialViewController, animated: false)
        })
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    @IBAction func homeTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "Home")
        
        self.present(initialViewController, animated: false)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
