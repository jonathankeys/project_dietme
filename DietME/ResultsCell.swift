//
//  ResultsCell.swift
//  DietME
//
//  Created by Fallah on 4/1/17.
//  Copyright © 2017 Wentworth. All rights reserved.
//

import UIKit

protocol ResultsCellProtocol {
    func didTapStepper(step: Double, atIndex: IndexPath)
}

class ResultsCell: UITableViewCell {

    @IBOutlet weak var foodImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var calories: UILabel!
    @IBOutlet weak var servingSize: UILabel!
    
    @IBOutlet weak var stepper: UIStepper!
    
    var indexPath: IndexPath!
    
    var delegate: ResultsCellProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func initCell(food: Food, index: IndexPath)  {
        backgroundColor = UIColor.clear
        stepper.value = Double(food.servingQty!)
        indexPath = index
        self.selectionStyle = .none
        foodImage.downloadedFrom(link: food.photo.thumb)
        name.text = " \(food.servingQty!) \(food.servingUnit!) \(food.foodName!)"
        calories.text = "Calories: \(food.nfCalories!)"
        servingSize.text = "Serving Size: \(food.servingWeightGrams!) grams"
        
        
    }
    
    @IBAction func didTapStepper(_ sender: UIStepper) {
        delegate?.didTapStepper(step: sender.value, atIndex: indexPath)
    }
    
}
