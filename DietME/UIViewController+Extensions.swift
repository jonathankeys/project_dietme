//
//  UIViewController+Extensions.swift
//  DietME
//
//  Created by Fallah on 4/2/17.
//  Copyright © 2017 Wentworth. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    open override var preferredStatusBarStyle:  UIStatusBarStyle {
        if let rootViewController = self.viewControllers.first {
            return rootViewController.preferredStatusBarStyle
        }
        return super.preferredStatusBarStyle
    }
    
    open override func viewDidLoad() {
//        let font = UIFont(name: "din-next-lt-pro", size: 16)
//        UIBarButtonItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName : UIColor.white, NSFontAttributeName: font! ], for: .normal)
    }
}


extension UIViewController {
    
    func setBackground()  {
        self.view.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "Mask"))
    }
    
    //-------------------
    // MARK: - Show Alert
    //-------------------
    func showErrorAlert(title: String, msg: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let action = UIAlertAction(title: "Got it.", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    func showErrorAlert(title: String, msg: String, completion:@escaping () -> Void) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
//        let action = UIAlertAction(title: "Got it.", style: .default, handler: )
        let action = UIAlertAction(title: "Got it", style: .default) { (alert: UIAlertAction) in
            completion()
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
//
    
    func showAlert(title: String, msg: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        present(alert, animated: true, completion: nil)
    }

    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
