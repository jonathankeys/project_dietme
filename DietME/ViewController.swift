//
//  ViewController.swift
//  DietME
//
//  Created by Fallah on 3/21/17.
//  Copyright © 2017 Wentworth. All rights reserved.
//

import Alamofire
import UIKit
import HealthKit
import SwiftSpinner
import Speech

import AVFoundation


class ViewController: UIViewController, SFSpeechRecognizerDelegate, ChickenViewDelegate {
    @IBOutlet weak var textView: UITextView!

    var dataFromApi: DietAPI?
    let manager = HealthManager()
    
    private var speechRecognizer: SFSpeechRecognizer!
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest!
    private var recognitionTask: SFSpeechRecognitionTask!
    private let audioEngine = AVAudioEngine()
    
    var player: AVAudioPlayer?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    func playSound() {
        guard let url = Bundle.main.url(forResource: "bite", withExtension: "mp3") else {
            print("url not found")
            return
        }
        
        do {
            /// this codes for making this app ready to takeover the device audio
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            /// change fileTypeHint according to the type of your audio file (you can omit this)
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3)
            player?.prepareToPlay()
            // no need for prepareToPlay because prepareToPlay is happen automatically when calling play()
            player!.play()
        } catch let error as NSError {
            print("error: \(error.localizedDescription)")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setBackground()
        speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))
        speechRecognizer.delegate = self
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.micButton.isUserInteractionEnabled = false
        
        self.textView.text = "Hit the button below and tell us what you ate today, then hit it again to stop"
        getPermissions()
        playSound()
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getPermissions()
    }
    
    private func getPermissions() {
        SFSpeechRecognizer.requestAuthorization { authStatus in
            /*
             The callback may not be called on the main thread. Add an
             operation to the main queue to update the record button's state.
             */
            OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized:
                    self.manager.authorizeHealthKit
                        { (success: Bool, error: NSError?) in
                            
                            if !success {
                                self.showErrorAlert(title: "Error", msg: "You must enable HealthKit in order to use this app", completion: {
                                    self.getPermissions()
                                })
                            } else if error != nil {
                                self.showErrorAlert(title: "Uh oh", msg: (error?.localizedDescription)!)
                            } else {
                                self.micButton.chickenViewDelegate = self
                                self.micButton.isUserInteractionEnabled = true
                            }
                    }
                    
                    
                case .denied:
                    OperationQueue.main.addOperation {
//                        self.recordBtn.isEnabled = false
                        self.micButton.isUserInteractionEnabled = false
//                        self.recordBtn.setTitle("User denied access to speech recognition", for: .disabled)
                        self.showErrorAlert(title: "Please enable Dictation", msg: "You must enable all permissions in order to use this app", completion: {
                            self.getPermissions()
                        })

                    }
                case .restricted:
                    OperationQueue.main.addOperation {
                        self.micButton.isUserInteractionEnabled = false
//                        self.recordBtn.setTitle("Speech recognition restricted on this device", for: .disabled)
                        self.showErrorAlert(title: "Uh oh", msg: " Your device is resticted, you cannot use this app.")
                    }
                    
                case .notDetermined:
                    OperationQueue.main.addOperation {
                        self.micButton.isUserInteractionEnabled = false
//                        self.recordBtn.isEnabled = false
//                        self.recordBtn.setTitle("Speech recognition not yet authorized", for: .disabled)
                    }
                }
            }
        }
    }
    @IBOutlet weak var micButton: ChickenView!
    
    private func hideStuffAndLoad() {
        textView.alpha = 0.0
        
        micButton.addLoadingAnimation()
    }
    
    func buttonChickenLegPressed(buttonChickenLeg: UIButton) {
        
        if audioEngine.isRunning {
            audioEngine.stop()
            micButton.removeAllAnimations()
            recognitionRequest?.endAudio()
            self.micButton.isUserInteractionEnabled = false
            
            if textView.text != "(listening...)" {
//                SwiftSpinner.show("Fetching Food")
                micButton.addLoadingAnimation()
                HealthLoggerUTIL.searchForFood(foodFromSpeech: textView.text) {
                    (apiResult: DietAPI?) in
                    
                    defer {
//                        SwiftSpinner.hide()
                        self.micButton.removeAllAnimations()
                    }
                    
                    guard let result = apiResult else {
                        //SHOW AN ERROR
                        self.textView.text = "Hit the button below and tell us what you ate today, then hit it again to stop"
                        return
                    }
                    
                    self.dataFromApi = result
                    if result.foods.isEmpty {
                        self.textView.text = "Hit the button below and tell us what you ate today, then hit it again to stop"
                        self.micButton.isUserInteractionEnabled = true
                        self.showErrorAlert(title: "Uh oh", msg: "We couldn't process your request, please try again")
                    } else {
                        self.performSegue(withIdentifier: "ResultsSegue", sender: self)
                    }
                    
                }
            }
            
            
        } else {
            
            micButton.addMicSpeakingAnimation()
            try! self.startRecording()
        }
    }
    
    
    private func startRecording() throws {
        
        // Cancel the previous task if it's running.
        if let recognitionTask = recognitionTask {
            recognitionTask.cancel()
            self.recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        try audioSession.setCategory(AVAudioSessionCategoryRecord)
        try audioSession.setMode(AVAudioSessionModeMeasurement)
        try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        guard let inputNode = audioEngine.inputNode else { fatalError("Audio engine has no input node") }
        guard let recognitionRequest = recognitionRequest else { fatalError("Unable to create a SFSpeechAudioBufferRecognitionRequest object") }
        
        // Configure request so that results are returned before audio recording is finished
        recognitionRequest.shouldReportPartialResults = true
        
        // A recognition task represents a speech recognition session.
        // We keep a reference to the task so that it can be cancelled.
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest) { result, error in
            var isFinal = false
            
            if let result = result {
                self.textView.text = result.bestTranscription.formattedString
                isFinal = result.isFinal
            }
            
            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
//                self.textView.text = "Hit the button below and tell us what you ate today, then hit the button again to stop"
                
                if self.textView.text == "(listening...)" {
                    self.textView.text = "Hit the button below and tell us what you ate today, then hit it again to stop"
                    self.micButton.removeAllAnimations()
                }
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                self.micButton.isUserInteractionEnabled = true
            }
        }
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        
        try audioEngine.start()
        
        textView.text = "(listening...)"
        
    }


    // MARK: - SFSpeechRecognizerDelegate
    
    public func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            self.textView.text = "Hit the button below and tell us what you ate today, then hit the button again to stop"
            self.micButton.isUserInteractionEnabled = true
        } else {
            self.micButton.isUserInteractionEnabled = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ResultsSegue" {
            let nvc = segue.destination as! UINavigationController 
            let dvc = nvc.topViewController as! ResultsViewController
            dvc.apiData = self.dataFromApi
            
        }
    }


}

