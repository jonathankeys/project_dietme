//
//  HistoryHeaderCell.swift
//  DietME
//
//  Created by Fallah on 4/2/17.
//  Copyright © 2017 Wentworth. All rights reserved.
//

import UIKit

protocol HistoryHeaderCellProtocol {
    func didTapBack()
    func didTapForward()
}

class HistoryHeaderCell: UITableViewCell {

    @IBOutlet weak var nextDayButton: UIButton!
    @IBOutlet weak var previousDayButton: UIButton!
    @IBOutlet weak var currentDateLabel: UILabel!
    
    var delegate: HistoryHeaderCellProtocol?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func initCell(date: Date) {
    
    }
    
    @IBAction func nextDayTapped(_ sender: Any) {
        delegate?.didTapForward()
    }
    
    @IBAction func previousDayTapped(_ sender: Any) {
        delegate?.didTapBack()
    }
}
