//
//  String+Extensions.swift
//  DietME
//
//  Created by Fallah on 4/1/17.
//  Copyright © 2017 Wentworth. All rights reserved.
//

import Foundation
import UIKit
public extension UIImageView {
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async(execute: {
                self.image = image
                
            })
            }.resume()
    }
}
