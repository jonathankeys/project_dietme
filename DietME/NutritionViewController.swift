//
//  NutritionViewController.swift
//  DietME
//
//  Created by Fallah on 4/2/17.
//  Copyright © 2017 Wentworth. All rights reserved.
//

import UIKit

class NutritionViewController: UITableViewController {

    let resultReuseID = "NutritionCell"
     var food: Food!
    var dataToDisplay = [(leftDetail: String?, rightDetail: String?)]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "NutritionCell", bundle: nil), forCellReuseIdentifier: resultReuseID)
        self.title = "Nutritional Facts"
        self.navigationController?.setToolbarHidden(true, animated: true)
        dataToDisplay = initFoodData(food: food)
        
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataToDisplay.count
    }
    
    private func initFoodData(food: Food) -> [(leftDetail: String?, rightDetail: String?)]  {
        var data = [(leftDetail: String?, rightDetail: String?)]()
        data.append(("\(food.foodName!)", ""))
        data.append((nil, nil)) //Black Rows
        //JON or Colby calculate calories from fat
        if food.nfCalories != nil {
            data.append(("Calories \(food.nfCalories!)", ""))
        } else {
            data.append(("Calories \(0)", ""))
        }
        
        if food.nfTotalFat != nil {
            data.append(("Total Fat \(food.nfTotalFat!)g", ""))
        } else {
            data.append(("Total Fat \(0)g", ""))
        }
        
        if food.nfSaturatedFat != nil {
            data.append(("\t Saturated Fat \(food.nfSaturatedFat!)g", ""))
        } else {
            data.append(("\t Saturated Fat \(0)g", ""))
        }
        
        if food.nfCholesterol != nil {
            data.append(("Cholesterol \(food.nfCholesterol!)mg", ""))
        } else {
            data.append(("Cholesterol \(0)mg", ""))
        }
        
        if food.nfSodium != nil {
            data.append(("Sodium \(food.nfSodium!)mg", ""))
        } else {
            data.append(("Sodium \(0)mg", ""))
        }
        
        if food.nfTotalCarbohydrate != nil {
            data.append(("Total Carbohydrates \(food.nfTotalCarbohydrate!)g", ""))
        } else {
            data.append(("Total Carbohydrates \(0)g", ""))
        }
        
        
        if food.nfPotassium != nil {
            data.append(("Potassium \(food.nfPotassium!)mg", ""))
        } else {
            data.append(("Potassium \(0)mg", ""))
        }
        
        if food.nfDietaryFiber != nil {
            data.append(("\t Dietary Fiber \(food.nfDietaryFiber!)g", ""))
        } else {
            data.append(("\t Dietary Fiber \(0)g", ""))
        }
        
        if food.nfSugars != nil {
            data.append(("\t Sugar \(food.nfSugars!)g", ""))
        } else  {
            data.append(("\t Sugar \(0)g", ""))
        }
        
        if food.nfProtein != nil {
            data.append(("Protein \(food.nfProtein!)g", ""))
        } else {
             data.append(("Protein \(0)g", ""))
        }

        return data
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: resultReuseID, for: indexPath) as! NutritionCell
        
        let dataRow = dataToDisplay[indexPath.row]
        
        guard let left = dataRow.leftDetail,
            let right = dataRow.rightDetail  else {
            cell.backgroundColor = UIColor.black
            return cell
        }
        
        cell.leftDetail.text = left
        cell.rightDetail.text = right
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1 {
            return 10.0
        } else {
            return 51.0
        }
    }

}
