//
//  HealthManager.swift
//  DietME
//
//  Created by Fallah on 3/27/17.
//  Copyright © 2017 Wentworth. All rights reserved.
//

import Foundation
import HealthKit


class HealthManager {
    
    private static let healthKitStore: HKHealthStore = HKHealthStore()
    
    func authorizeHealthKit(completion: @escaping (_ success:Bool,_ error: NSError?) -> Void)
    {
//        let healthKitTypesToRead: Set<HKSampleType> = [
//            HKObjectType.quantityType(forIdentifier: .dietaryFiber)!,
//            HKObjectType.quantityType(forIdentifier: .dietaryEnergyConsumed)!,
//            HKObjectType.quantityType(forIdentifier: .dietaryPotassium)!,
//            HKObjectType.quantityType(forIdentifier: .dietarySugar)!,
//            HKObjectType.quantityType(forIdentifier: .dietaryFatTotal)!,
//            HKObjectType.quantityType(forIdentifier: .dietaryFatSaturated)!,
//            HKObjectType.quantityType(forIdentifier: .dietaryCholesterol)!,
//            HKObjectType.quantityType(forIdentifier: .dietarySodium)!,
//            HKObjectType.quantityType(forIdentifier: .dietaryCarbohydrates)!,
//            HKObjectType.quantityType(forIdentifier: .dietaryProtein)!
//        ]
        // 2. Set the types you want to write to HK Store
        let healthKitTypesToWrite: Set<HKSampleType> = [
            HKObjectType.quantityType(forIdentifier: .dietaryFiber)!,
            HKObjectType.quantityType(forIdentifier: .dietaryEnergyConsumed)!,
            HKObjectType.quantityType(forIdentifier: .dietaryPotassium)!,
            HKObjectType.quantityType(forIdentifier: .dietarySugar)!,
            HKObjectType.quantityType(forIdentifier: .dietaryFatTotal)!,
            HKObjectType.quantityType(forIdentifier: .dietaryFatSaturated)!,
            HKObjectType.quantityType(forIdentifier: .dietaryCholesterol)!,
            HKObjectType.quantityType(forIdentifier: .dietarySodium)!,
            HKObjectType.quantityType(forIdentifier: .dietaryCarbohydrates)!,
            HKObjectType.quantityType(forIdentifier: .dietaryProtein)!,
            HKObjectType.quantityType(forIdentifier: .dietaryEnergyConsumed)!
        ]
        
        // 3. If the store is not available (for instance, iPad) return an error and don't go on.
        if !HKHealthStore.isHealthDataAvailable()
        {
            let error = NSError(domain: "WIT.DietME", code: 2, userInfo: [NSLocalizedDescriptionKey:"HealthKit is not available in this Device"])
            
            completion(false,error)

            return
        }
        
        
        HealthManager.healthKitStore.requestAuthorization(toShare:
            healthKitTypesToWrite, read: nil)
        { (success: Bool, error: Error?) in
            
            if !self.didAllowAnyPermission(typesToWrite: healthKitTypesToWrite) {
                completion(false,nil)
            } else if error == nil {

                
                completion(success,nil)
            }
            
            return
        }
        
        
     
    }
    
    private func didAllowAnyPermission(typesToWrite: Set<HKSampleType>) -> Bool {
        var allTypesNotAllowed = [HKSampleType]()
        let types = Array(typesToWrite)
        for type in types {
            if HealthManager.healthKitStore.authorizationStatus(for: type) != .sharingAuthorized {
                allTypesNotAllowed.append(type)
            }
        }

        if allTypesNotAllowed.count == types.count {
            return false
        } else {
            return true
        }
        
    }
    
//    completion: @escaping (_ success:Bool,_ error: NSError?) -> Void
    static func allowedAll(healthKitTypes: Set<HKSampleType>, completion: @escaping (Bool,HKSampleType?) -> Void)
    {
        let types = Array(healthKitTypes)
        for type in types {
            if HealthManager.healthKitStore.authorizationStatus(for: type) != .sharingAuthorized {
                completion(false,type)
                return
            }
        }
        
        completion(true,nil)
    }
    
    
    static func logFoodToHealthKit(food: Food) {
        if let calories = food.nfCalories, food.nfCalories != 0,
            food.foodName != nil, food.foodName != "" {
                writeCalorieSample(calories: calories, name: food.foodName)
        }
        if let totalFat = food.nfTotalFat, food.nfTotalFat != 0,food.foodName != nil, food.foodName != "" {
            writeFatSample(fat: totalFat, name: food.foodName)
        }
        
        if let carbs = food.nfTotalCarbohydrate, food.nfTotalCarbohydrate != 0 ,food.foodName != nil, food.foodName != "" {
            writeCarbSample(carb: carbs, name: food.foodName)
        }
        
        if let sugar = food.nfSugars, food.nfSugars != 0 ,food.foodName != nil, food.foodName != "" {
            writeSugarSample(sugar: sugar, name: food.foodName)
        }
    
        if let fiber = food.nfDietaryFiber, food.nfDietaryFiber != 0 ,food.foodName != nil, food.foodName != "" {
            writeFiberSample(fiber: fiber, name: food.foodName)
        }
        
        if let protein = food.nfProtein, food.nfProtein != 0 ,food.foodName != nil, food.foodName != "" {
            writeProteinSample(protein: protein, name: food.foodName)
        }
        
        if let potassium = food.nfPotassium, food.nfPotassium != 0 ,food.foodName != nil, food.foodName != "" {
                writePotassiumSample(potassium: potassium, name: food.foodName)
        }
        
        if let sodium = food.nfSodium, food.nfSodium != 0 ,food.foodName != nil, food.foodName != "" {
            writeSodiumSample(sodium: sodium, name: food.foodName)
        }
        
        if let cholesterol = food.nfCholesterol, food.nfCholesterol != 0 ,food.foodName != nil, food.foodName != "" {
            writeCholestoralSample(cholestoral: cholesterol, name: food.foodName)
        }
        
        if let satFat = food.nfSaturatedFat, food.nfSaturatedFat != 0 ,food.foodName != nil, food.foodName != "" {
            writeFatSaturatedSample(fatSat: satFat, name: food.foodName)
        }
        
    }
    
    static func writeFiberSample(fiber: Float, name: String) {
        // 1. Create a  Sample
        let fiberType = HKQuantityType.quantityType(forIdentifier: .dietaryFiber)
        let fiberQuantity = HKQuantity(unit: HKUnit.gram(), doubleValue: Double(fiber))
        let fiberSample = HKQuantitySample(type: fiberType!, quantity: fiberQuantity, start: Date(), end: Date())
        
        // 2. Save the sample in the store
        HealthManager.healthKitStore.save(fiberSample, withCompletion:
            { (success, error) -> Void in
            if( error != nil ) {
                print("Error saving Fiber sample: \(String(describing: error?.localizedDescription))")
            } else {
                print("Fiber sample saved successfully!")
            }
        })
    }
    
    static func writeCalorieSample(calories: Float, name: String) {
        // 1. Create a  Sample
        let calorieType = HKQuantityType.quantityType(forIdentifier: .dietaryEnergyConsumed)
        let caloriesQuantity = HKQuantity(unit: HKUnit.calorie(), doubleValue: Double(calories)*1000)
        let calorieSample = HKQuantitySample(type: calorieType!, quantity: caloriesQuantity, start: Date(), end: Date(),metadata: ["Food": name] )

        // 2. Save the sample in the store
        HealthManager.healthKitStore.save(calorieSample, withCompletion:
            { (success, error) -> Void in
            if( error != nil ) {
                print("Error saving calorie sample: \(String(describing: error?.localizedDescription))")
            } else {
                print("Calorie sample saved successfully!")
            }
        })
    }
    
    static func writePotassiumSample(potassium: Float, name: String) {
        let potassiumType = HKQuantityType.quantityType(forIdentifier: .dietaryPotassium)
        let potassiumQuantity = HKQuantity(unit: HKUnit.gramUnit(with: HKMetricPrefix.milli), doubleValue: Double(potassium) )
        
        
        let potassiumSample = HKQuantitySample(type: potassiumType!, quantity: potassiumQuantity, start: Date(), end: Date(), metadata: ["Food": name])
        
        HealthManager.healthKitStore.save(potassiumSample, withCompletion:
            { (success, error) -> Void in
            if( error != nil ) {
                print("Error saving Pottassium sample: \(String(describing: error?.localizedDescription))")
            } else {
                print("Potassium sample saved successfully!")
            }
        })
    }
    
    static func writeSugarSample(sugar: Float,name: String) {
        let sugarType = HKQuantityType.quantityType(forIdentifier: .dietarySugar)
        let sugarQuantity = HKQuantity(unit: HKUnit.gram(), doubleValue: Double(sugar))
        let sugarSample = HKQuantitySample(type: sugarType!, quantity: sugarQuantity, start: Date(), end: Date(),metadata: ["Food": name])
     
        HealthManager.healthKitStore.save(sugarSample, withCompletion:
            { (success, error) -> Void in
                if( error != nil ) {
                    print("Error saving Sugar sample: \(String(describing: error?.localizedDescription))")
                } else {
                    print("Sugar sample saved successfully!")
                }
        })
    }
    
    static func writeFatSample(fat: Float, name: String) {
        let fatType = HKQuantityType.quantityType(forIdentifier: .dietaryFatTotal)
        let fatQuantity = HKQuantity(unit: HKUnit.gram(), doubleValue: Double(fat))
        let fatSample = HKQuantitySample(type: fatType!, quantity: fatQuantity, start: Date(), end: Date(),metadata: ["Food": name])
        
        HealthManager.healthKitStore.save(fatSample, withCompletion:
            { (success, error) -> Void in
                if( error != nil ) {
                    print("Error saving Fat sample: \(String(describing: error?.localizedDescription))")
                } else {
                    print("Fat Saturated sample saved successfully!")
                }
        })
    }
    
    static func writeFatSaturatedSample(fatSat: Float, name: String) {
        let fatSatType = HKQuantityType.quantityType(forIdentifier: .dietaryFatSaturated)
        let fatSatQuantity = HKQuantity(unit: HKUnit.gram(), doubleValue: Double(fatSat))
        let fatSample = HKQuantitySample(type: fatSatType!, quantity: fatSatQuantity, start: Date(), end: Date(),metadata: ["Food": name])
        
        HealthManager.healthKitStore.save(fatSample, withCompletion:
            { (success, error) -> Void in
                if( error != nil ) {
                    print("Error saving Sugar sample: \(String(describing: error?.localizedDescription))")
                } else {
                    print("Sugar sample saved successfully!")
                }
        })
    }
    
    static func writeCholestoralSample(cholestoral: Float, name :String) {
        let type = HKQuantityType.quantityType(forIdentifier: .dietaryCholesterol)
        let quantity = HKQuantity(unit: HKUnit.gramUnit(with: HKMetricPrefix.milli), doubleValue: Double(cholestoral))
        let sample = HKQuantitySample(type: type!, quantity: quantity, start: Date(), end: Date(),metadata: ["Food": name])
        
        HealthManager.healthKitStore.save(sample, withCompletion:
            { (success, error) -> Void in
                if( error != nil ) {
                    print("Error saving Cholestoral sample: \(String(describing: error?.localizedDescription))")
                } else {
                    print("Sugar sample Cholestoral successfully!")
                }
        })
    }
    
    static func writeSodiumSample(sodium: Float,name : String) {
        let type = HKQuantityType.quantityType(forIdentifier: .dietarySodium)
        let quantity = HKQuantity(unit: HKUnit.gramUnit(with: HKMetricPrefix.milli), doubleValue: Double(sodium))
        let sample = HKQuantitySample(type: type!, quantity: quantity, start: Date(), end: Date(),metadata: ["Food" : name])
        
        HealthManager.healthKitStore.save(sample, withCompletion:
            { (success, error) -> Void in
                if( error != nil ) {
                    print("Error saving Sodium sample: \(String(describing: error?.localizedDescription))")
                } else {
                    print("Sodium sample saved successfully!")
                }
        })
    }
    
    static func writeCarbSample(carb: Float,name : String) {
        let type = HKQuantityType.quantityType(forIdentifier: .dietaryCarbohydrates)
        let quantity = HKQuantity(unit: HKUnit.gram(), doubleValue: Double(carb))
        let sample = HKQuantitySample(type: type!, quantity: quantity, start: Date(), end: Date(),metadata: ["Food": name])
        
        HealthManager.healthKitStore.save(sample, withCompletion:
            { (success, error) -> Void in
                if( error != nil ) {
                    print("Error saving \(String(describing: type?.identifier)) sample: \(String(describing: error?.localizedDescription))")
                } else {
                    print("\(String(describing: type?.identifier)) sample saved successfully!")
                }
        })
    }
    
    static func writeVitaminASample(vitaminA: Float,name: String) {
        let type = HKQuantityType.quantityType(forIdentifier: .dietaryVitaminA)
        let quantity = HKQuantity(unit: HKUnit.gram(), doubleValue: Double(vitaminA))
        let sample = HKQuantitySample(type: type!, quantity: quantity, start: Date(), end: Date(), metadata: ["Food": name])
        
        HealthManager.healthKitStore.save(sample, withCompletion:
            { (success, error) -> Void in
                if( error != nil ) {
                    print("Error saving \(String(describing: type?.identifier)) sample: \(String(describing: error?.localizedDescription))")
                } else {
                    print("\(String(describing: type?.identifier)) sample saved successfully!")
                }
        })
    }
    
    static func writeProteinSample(protein: Float,name: String) {
        let type = HKQuantityType.quantityType(forIdentifier: .dietaryProtein)
        let quantity = HKQuantity(unit: HKUnit.gram(), doubleValue: Double(protein))
        let sample = HKQuantitySample(type: type!, quantity: quantity, start: Date(), end: Date())
        
        HealthManager.healthKitStore.save(sample, withCompletion:
            { (success, error) -> Void in
                if( error != nil ) {
                    print("Error saving \(String(describing: type?.identifier)) sample: \(String(describing: error?.localizedDescription))")
                } else {
                    print("\(String(describing: type?.identifier)) sample saved successfully!")
                }
        })
    }
    
    static func writeVitaminCSample(vitaminC: Float, name: String) {
        let type = HKQuantityType.quantityType(forIdentifier: .dietaryVitaminC)
        let quantity = HKQuantity(unit: HKUnit.gram(), doubleValue: Double(vitaminC))
        let sample = HKQuantitySample(type: type!, quantity: quantity, start: Date(), end: Date(), metadata: ["Food": name])
        
        HealthManager.healthKitStore.save(sample, withCompletion:
            { (success, error) -> Void in
                if( error != nil ) {
                    print("Error saving \(String(describing: type?.identifier)) sample: \(String(describing: error?.localizedDescription))")
                } else {
                    print("\(String(describing: type?.identifier)) sample saved successfully!")
                }
        })
    }
    
    static func getFoodNamesToday(completion : @escaping (_ foodNames: [String]) -> Void) {
        let calendar = NSCalendar.current
        let now = Date()

        let components = calendar.dateComponents([.year,.month,.day], from: now)
        
        guard let startDate = calendar.date(from: components) else {
            fatalError("*** Unable to create the start date ***")
        }

        let endDate = calendar.date(byAdding: .day, value: 1, to: now)
        
        
        guard let sampleType = HKSampleType.quantityType(forIdentifier: .dietaryEnergyConsumed) else {
            fatalError("*** This method should never fail ***")
        }

        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: .strictEndDate)
        
        var foods = [String]()
        
        let query = HKSampleQuery(sampleType: sampleType, predicate: predicate, limit: Int(HKObjectQueryNoLimit), sortDescriptors: nil) {
            query, results, error in
            
            
            guard let samples = results as? [HKQuantitySample] else {
                fatalError("An error occured fetching the user's tracked food. In your app, try to handle this error gracefully. The error was: \(String(describing: error?.localizedDescription))");
            }
            
            
            
            for sample in samples {
                guard let foodName = sample.metadata?["Food"] as? String else {
                        // if the metadata doesn't record the food type, just skip it.
                        continue
                }
                foods.append(foodName)
            }
            completion(foods)
            
        }
        
        healthKitStore.execute(query)
    
    }
    
    static func getFoodNamesFromDay(day: Date, completion : @escaping (_ foodNames: [String]) -> Void) {
        let calendar = NSCalendar.current
        
        let components = calendar.dateComponents([.year,.month,.day], from: day)
        
        guard let startDate = calendar.date(from: components) else {
            fatalError("*** Unable to create the start date ***")
        }
        
        let endDate = calendar.date(byAdding: .day, value: 1, to: day)
        
        
        guard let sampleType = HKSampleType.quantityType(forIdentifier: .dietaryEnergyConsumed) else {
            fatalError("*** This method should never fail ***")
        }
        
        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: .strictEndDate)
        
        var foods = [String]()
        
        let query = HKSampleQuery(sampleType: sampleType, predicate: predicate, limit: Int(HKObjectQueryNoLimit), sortDescriptors: nil) {
            query, results, error in
            
            
            guard let samples = results as? [HKQuantitySample] else {
                fatalError("An error occured fetching the user's tracked food. In your app, try to handle this error gracefully. The error was: \(String(describing: error?.localizedDescription))");
            }
            
            
            
            for sample in samples {
                guard let foodName = sample.metadata?["Food"] as? String else {
                    // if the metadata doesn't record the food type, just skip it.
                    continue
                }
                foods.append(foodName)
            }
            completion(foods)
            
        }
        
        healthKitStore.execute(query)
        
    }

    
    
}
